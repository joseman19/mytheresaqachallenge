
Feature('Search recipe');

Scenario('Search cookie monster cupcakes on the search bar', (I) => {
    I.amOnPage('https://www.food2fork.com/');
    I.see('Food2Fork makes it easy to find great recipes');
    I.waitForElement('input#typeahead', 10);
    I.fillField('input#typeahead', 'cookie monster cupcakes')
    I.click('.icon-search')
    I.waitForVisible('.recipe-name');
    I.see('Cookie Monster cupcakes','.recipe-name');
});
