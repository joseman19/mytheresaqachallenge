var api_util = require('../util-rest-api');
var assert = require('assert');

Feature('Api search test');

Scenario('Test that the API REST for searching cookie monster cupcakes works well', async(I) => {
    const response = await I.sendGetRequest('/api/search?key=e676ea4152b2077f7e7bef634e232fff&q=cookie%20monster%20cupcakes')
    assert.equal(response.status, '200');

    if(response.data.error == "limit") {
        console.log("Limit of 50 api free requests reached!!")
        assert.equal(response.data.error,"cookie monster cupcakes");
    }
    else {
        var recipe = api_util.searchname(response.data.recipes, 'cookie monster cupcakes');

        if (recipe == " "){
            console.log("the search string is not found!!");
        }
        else {
            assert.equal(recipe,"cookie monster cupcakes");
        }
    }
});
