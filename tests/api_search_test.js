var should = require("should");
var request = require("request");
var expect = require("chai").expect;

var util= require("util");
var api_util = require('../util-rest-api');
   
['cookie%20monster%20cupcakes','cookie%20monster','cookie'].forEach(function (searchString) {
    describe('Search api testing with the next query string ' + searchString, function() {
        it ('The Api returns 200 Ok and the title of the searched recipe is in the list returned', function(done) {
            var options = api_util.configureoptions(searchString);
            request(options, function (error, response, body) {
                    var bodyObj = JSON.parse(body);
                    expect(response.statusCode).to.equal(200);

                    if (bodyObj.error == "limit")
                        done(new Error("Limit of 50 api free requests reached!!"));

                    var recipe= api_util.searchname(bodyObj.recipes, 'cookie monster cupcakes');

                    if (recipe == " "){
                        done(new Error("the search string is not found!!"));
                    }
                    else {
                        expect(recipe).to.equal("cookie monster cupcakes");
                        done();
                    }

                    if (error) throw new Error(error);

            });
            
        });
    });
});