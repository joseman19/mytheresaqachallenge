## MyTheresa QA Challenge

Project based on the three practical questions of the challenge to opt to enter as a quality control engineer at MyTheresa.

**The three practical questions are:**

With the coding language and framework you feel more comfortable:

1.- Test that in: ​https://www.food2fork.com/​ when you search for “cookie monster cupcakes” the search is well performed.

2.- Test that using the API: ​https://www.food2fork.com/about/api​ and the following API key: e676ea4152b2077f7e7bef634e232fff the same search is well performed.


3.- With the coding language you feel more comfortable write a method that passing a number as an argument:    

Returns ‘’my” when the number is dividable by 3.

Returns “theresa” when the number is dividable by 5.

Returns “mytheresa” when the number is dividable by 3 and 5.

Returns the given number when the previous rules are not met.                                                                                                         
Take into account when implementing the method that in a near future we would like to add more rules in an easy way. For example:         

Return “clothes” when the number is dividable by 7.

Return “myclothes” when the number is dividable by 3 and 7 and so on.


**Tools or frameworks I used:**

1.- *CodeceptJS*: CodeceptJS is a modern end to end testing framework with a special BDD-style syntax. It is compatible with many engines such as Puppeter or Protactor and is very configurable. I chose to use the puppet because of the speed in the execution of the test when implementing the test of question # 1. I choose to implement the API test of question # 2 with this framework although it has to be said that it is a framework designed to implement functional tests or E2E.

2.- *Mocha*: Mocha is a feature-rich JavaScript test framework running on Node.js and in the browser, making asynchronous testing simple and fun. Mocha tests run serially, allowing for flexible and accurate reporting, while mapping uncaught exceptions to the correct test cases. I have opted to use this framework to make an another implementation of the question #2 with two more posible queries to test. I use request package to make the API requests and Chai as a assertion library for the test.

3.- *Allure*: For reporting the test implemented in the codeception framework.

4.- *Mochawesome*: For reporting the test implemented  inmocha.

**Requeriments:**

In order to install and execute the proyect you have to install( I specify the installed version I have used):

1.- *NodeJS v10.15.3*

**Steps to install:**

1.- Once you have cloned the proyect you only have to execute the command `npm install` in the root proyect to install all the required nodejs dependencies.

## How to execute the tests

Next, you’ll find how to execute the diferent tests I have implemented. All this commands have to be executed in the root proyect.

`npm run e2e-testing` : To execute the E2E test implemented for the question #1 using CodeceptJS.

`npm run api-testing` : To execute the API Rest test implemented for the question #2 using CodeceptJS.

`npm run generate-report-codeception` : To generate report for the tests executed using CodeceptJS.

`npm run api-testing-mocha` : To execute the API Rest test implemented for the question #2 using Mocha.

`npm run generate-report-mocha` : To generate report for the tests executed using Mocha.

`npm run --number=<number_to_pass_to_the_method> test-dividable` : To execute the method implemented for the question #3. You have to pass the number by command line.

For example: `npm run --number=42 test-dividable`.
