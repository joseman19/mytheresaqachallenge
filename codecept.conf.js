exports.config = {
  tests: './tests/*_test.js',
  timeout: 10000,
  output: './output',
  helpers: {
    Puppeteer: {
      url: 'https://www.food2fork.com',
      waitForNavigation: "networkidle0",
      show: true
    },
    REST: {
        endpoint: "https://www.food2fork.com",
    }
  },
  include: {
    I: './steps_file.js'
  },
  plugins: {
    allure: {
      enabled: true
  }
  },
  bootstrap: null,
  mocha: {},
  name: 'mytheresa_qachallenge'
}