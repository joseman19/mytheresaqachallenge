module.exports = {
    searchname : function(x, somename){
        for(var i = 0; i < x.length; i++) {
            if (x[i]['title'].toLowerCase()==somename){
                return x[i]['title'].toLowerCase();
            }
        }
        return " "; //This means no match found
    },
    configureoptions : function(searchstring){
        var options = { method: 'GET',
        url: 'https://www.food2fork.com/api/search',
        qs: 
        { key: 'e676ea4152b2077f7e7bef634e232fff',
            q: searchstring },
        headers: 
        { 'cache-control': 'no-cache' } };
        return options;
    }
  };